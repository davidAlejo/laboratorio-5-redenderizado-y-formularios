import React, { useState } from 'react'
import Filter from './components/Filter'
import PersonForm from './components/PersonForm'
import Person from './components/Person'

const App = () => {
  const [ persons, setPersons ] = useState([
    { name: 'Arto Hellas',
      number: '951841512' }
  ])

  const [ newName, setNewName ] = useState('')
  const [ newNumber, setNewNumber ] = useState('')
  const [ filtro, setFiltro ] = useState('')

  const eventoCambioNombre = (event) => {
    setNewName(event.target.value)
  }

  const eventoNuevoNumero = (event) => {
    setNewNumber(event.target.value)
  }

  const filtroBusqueda = (event) => {
    setFiltro(event.target.value)
  }

  const eventoClick = (event) => {
    event.preventDefault()

    for (let j = 0; j < persons.length; j++) {
      if (newName === persons[j].name || newNumber === persons[j].number) {
        alert(`${newName} ya existe en su lista`)
        setPersons([...persons])
      }else if (newName === "" || newNumber === "") {
        alert(`Por favor, Ingrese un valor`)
        setPersons([...persons])
      } else {
        const person = {
          name: newName,
          number: newNumber
        }
        setPersons([...persons, person] )
        setNewName("")
        setNewNumber("")
      }
      
    }
  }

  return (
    <div>
      <h2>Phonebook</h2>
      <Filter filtroBusqueda={filtroBusqueda}/>
      <PersonForm eventoCambioNombre={eventoCambioNombre} eventoNuevoNumero={eventoNuevoNumero} eventoClick={eventoClick} newName={newName} newNumber={newNumber}/>
      
      <h2>Numbers</h2>
      <div>
        {persons.filter((persona) => persona.name.includes(filtro)).map((person) => <Person key={person.name} person={person}/>)}
      </div>
    </div>
  )
}

export default App
