import React from "react";

const PersonForm = ({eventoCambioNombre, eventoNuevoNumero, eventoClick, newName, newNumber}) => {
  return (
    <form onSubmit={eventoClick}>
      <h2>Add a new</h2>
      <div>
        name:{" "}
        <input type="text" onChange={eventoCambioNombre} value={newName} />
      </div>
      <div>
        Number:{" "}
        <input type="number" onChange={eventoNuevoNumero} value={newNumber} />
      </div>
      <div>
        <button type="submit">add</button>
      </div>
    </form>
  );
};

export default PersonForm;
