import React from 'react'

const Person = (props) => {
    return (
        <div>
            <p>{props.person.name}, con numero: {props.person.number}</p>
        </div>
    )
}

export default Person
