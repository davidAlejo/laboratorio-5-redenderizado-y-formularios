import Part from "./Part";

const Content = (props) => {
  const total = props.partes.reduce((a, { exercises }) => a + exercises, 0);
  return (
    <div>
      {props.partes.map((part) => <Part key={part.id} parte={part} /> )}
      <p>Suma total de ejercicios: {total}</p>
    </div>
  );
};

export default Content;
