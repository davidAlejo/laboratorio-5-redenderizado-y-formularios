import React from "react";
import Content from "./Content";
import Header from "./Header";

const Course = (props) => {
  return (
    <div style={{ marginLeft: "15px" }}>
      {props.courses.map((course) => {
        return (
          <div key={course.id}>
            <Header nombre={course.name} id={course.id} />
            <Content partes={course.parts} />
          </div>
        );
      })}
    </div>
  );
};

export default Course;
