const Header = (props) => {
  return (
    <div>
      <h3>
        {props.nombre}, con id: {props.id}
      </h3>
    </div>
  );
};

export default Header;
